<?php get_header(); ?>

<div id="content">
	
	<div class="wrapper wrapper-main">








		<div id="main">
		
			<div class="wrapper-content">



<h1 style="margin-bottom: 1em; line-height: 1.4;">
<?php $my_query = new WP_Query('tag=tagline&posts_per_page=1');
	while ($my_query->have_posts()) : $my_query->the_post(); ?>
	<?php the_title(); ?><?php endwhile; ?>
</h1>





			
				<?php if (is_home() && $paged < 2) { ?>

				<div class="academia-home-full">

				<?php get_template_part('slideshow-home'); } ?>
				
				<?php if (is_active_sidebar('home-full-1') && is_home() && $paged < 2) { ?>
				
				<?php
				if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage: Top') ) : ?> <?php endif;
				?>
				
				<?php } ?>
				
				<?php if (is_home() && $paged < 2) { ?>
				</div><!-- .academia-home-full -->
				<?php } ?>

				


<ul class="academia-posts">
		<?php
			$my_query = new WP_Query('cat=3');
			while ($my_query->have_posts()) : $my_query->the_post(); ?>


				<li <?php post_class('testimonials'); ?>>

		<?php
		get_the_image( array( 'size' => 'thumb-loop-main', 'width' => 260, 'before' => '<div class="post-cover">', 'after' => '</div><!-- end .post-cover -->' ) );
		?>
		
		<div class="post-content">
			<h2 class="title-post title-ms home"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'campus' ), the_title_attribute( 'echo=0' ) ); ?>"><?php the_title(); ?></a></h2>
			<p class="post-excerpt"><?php echo get_the_excerpt(); ?></p>
			<p class="post-meta"><time datetime="<?php echo get_the_date('c'); ?>" pubdate><?php echo get_the_date(); ?></time> / <span class="category"><?php the_category(', '); ?></span></p>
		</div><!-- end .post-content -->

		<div class="cleaner">&nbsp;</div>
		
	</li><!-- end .academia-post -->
	<?php endwhile; ?>

</ul>

	
				
			</div><!-- .wrapper-content -->
		
		</div><!-- #main -->
		
		<?php get_sidebar(); ?>
		
		<div class="cleaner">&nbsp;</div>
	</div><!-- .wrapper .wrapper-main -->

</div><!-- #content -->
	
<?php get_footer(); ?>